@objects
    header				           xpath     //*[@id = 'page-header']
    agoda-logo                     xpath     //*[@class = 'LogoAndLinksMenu__LogoContainer']
    login                          xpath     //*[@id = 'sign-in-btn']
    register                       xpath     //*[@id = 'sign-up-btn']
    hero-banner                    xpath     //*[@data-selenium = 'hero-banner-h1']
    search-box                     xpath     //*[@class = 'Searchbox__wrapper']
    location-box                   xpath     //*[@data-selenium = 'autocomplete-box']
    check-in-box                   xpath     //*[@data-element-name = 'check-in-box']
    occupancy-box                  xpath     //*[@data-element-name = 'occupancy-box']
    search-button                  xpath     //*[@data-selenium = 'searchButton']
    tile-container                 xpath     //*[@class = 'TileContainer__Contents']
    todays-deal-tile               xpath     //*[@data-selenium = 'todays-deal-tile']
    todays-deals-tile              xpath     //*[@data-selenium = 'todays-deals-tile']
    agoda-homes-tile               xpath     //*[@data-selenium = 'agoda-homes-tile']
    top-destination                xpath     //*[@class = 'TopDestination__Title']
    international-tab-content      xpath     //*[@id = 'international-tab-content']
    footer-help-column             xpath     //*[@data-selenium = 'footer-help-column']
    footer-about-us-column         xpath     //*[@data-selenium = 'footer-about-us-column']
    footer-destinations-column     xpath     //*[@data-selenium = 'footer-destinations-column']
    footer-partner-with-us-column  xpath     //*[@data-selenium = 'footer-partner-with-us-column']
    footer-mobile-app-column       xpath     //*[@data-selenium = 'footer-mobile-app-column']


= Home Page =
    @on desktop
        header:
            height 60px
            width 1440px
            inside screen 0px top
        agoda-logo:
            height 60px
            width 89px
            inside header 0px top
        login:
            text is "GIRIŞ YAPIN"
        register:
            text is "HESAP OLUŞTURUN"
        hero-banner:
            inside screen 103px top
            text is "OTELLER, RESORTLAR, HOSTELLER VE DAHA FAZLASI"
        search-box:
            height 249px
            width 580px
        location-box:
            aligned vertically left check-in-box
            aligned vertically left occupancy-box
        search-button:
            text is "ARA"
        tile-container:
            height ~ 670px
            width ~ 698px
        todays-deals-tile:
            aligned vertically left todays-deal-tile
            aligned vertically right agoda-homes-tile
        top-destination:
            text is "En popüler noktalar"
        international-tab-content:
            height ~ 147px
            width ~ 1180px
        footer-help-column:
            aligned horizontally top footer-about-us-column
            aligned horizontally top footer-about-us-column
            aligned horizontally top footer-destinations-column
            aligned horizontally top footer-partner-with-us-column
            aligned horizontally top footer-mobile-app-column