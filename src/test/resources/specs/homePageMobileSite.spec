@objects
        header				           xpath     //*[@data-component = 'HeaderComponent']
        agoda-logo                     xpath     //*[@class = 'LogoAndLinksMenu__LogoContainer']
        search-box                     xpath     //*[@data-element-name = 'mob-search-place']
        date-box                       xpath     //*[@data-element-name = 'mob-check-in-check-out']
        occupancy                      xpath     //*[@data-element-name = 'mob-occupancy']


= Home Page =
    @on desktop
        header:
            height 56px
            width 320px
            inside screen 89px top
        search-box:
            aligned vertically all date-box
            aligned vertically all occupancy