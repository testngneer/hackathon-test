@objects
        header				           id        page-header
        agoda-logo                     xpath     //*[@class = 'LogoAndLinksMenu__LogoContainer']
        login                          id        sign-in-btn
        register                       id        sign-up-btn
        search-box                     xpath     //*[@class = 'Searchbox Searchbox--horizontal']
        price-filter-range             id        PriceFilterRange
        starrating-filter-list         id        starratingFilterList
        location-score-filter-list     id        locationScoreFilterList
        filter-name-list               id        filterNameList
        static-map-container           id        staticMapContainer
        joined-filter-list             id        joinedFilterList
        recommended-same-country       id        recommendedSameCountry

= Home Page =
    @on desktop
        header:
            height 60px
            width 1440px
            inside screen 0px top
        agoda-logo:
            height 60px
            width 89px
            inside header 0px top
        login:
            text is "GIRIŞ YAPIN"
        register:
            text is "HESAP OLUŞTURUN"
        price-filter-range:
            aligned horizontally all starrating-filter-list
            aligned horizontally all location-score-filter-list
            aligned horizontally all filter-name-list
        static-map-container:
            aligned vertically all joined-filter-list
            aligned vertically all recommended-same-country
