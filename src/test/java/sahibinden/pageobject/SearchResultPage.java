package sahibinden.pageobject;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class SearchResultPage
{
    @FindBy(xpath = "//input[@id='price_box_1']")
    public WebElement hingPriceInput;

    @FindBy(xpath = "//span[@class='filter-item-content'][text()='Mükemmel']")
    public WebElement excellentComment;

    @FindBy(xpath = "//span[@class='filter-item-content'][text()='Ücretsiz iptal']")
    public WebElement freeCancellation;

    @FindBy(xpath = "//li[@id='transportationFilterList']//li[@class='filter-item-react']")
    public List<WebElement> transportationFilterList;

    @FindBy(xpath = "//li[@id='familyFilterId']//span[@class='filter-item-content']")
    public WebElement forFamilyFilter;

    @FindBy(xpath = "//section[@class='hotel-item-box']")
    public List<WebElement> hotelDetail;

    @FindBy(xpath = "//button[@class='ChildRoomsList-bookButtonInput']")
    public List<WebElement> nowBookButton;

    @FindBy(xpath = "//div[@class='Flights-Results-FlightPriceSection right-alignment']")
    public List<WebElement> searchResults;


}

