package sahibinden.pageobject;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class AgodaFlightsPage
{
    @FindBy(xpath = "//*[@name = 'destination']")
    public List<WebElement> destination;

    @FindBy(xpath = "//div[@class='monthDisplay']")
    public List<WebElement> dateName;

    @FindBy(xpath = "*//div[@aria-label='Nisan 20']")
    public WebElement holidayStart;

    @FindBy(xpath = "*//div[@aria-label='Nisan 23']")
    public WebElement holidayFinish;

    @FindBy(xpath = "//div[@aria-label='Next month']")
    public WebElement nextMonth;

    @FindBy(xpath = "//div[@aria-label = 'Gidiş tarihi']")
    public  WebElement go;

    @FindBy(xpath = "//div[@aria-label = 'Dönüş tarihi']")
    public  WebElement back;

    @FindBy(xpath = "//button[@aria-label = 'Ara']")
    public  WebElement searchButton;
}
