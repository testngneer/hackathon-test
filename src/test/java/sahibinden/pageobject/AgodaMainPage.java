package sahibinden.pageobject;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class AgodaMainPage
{
    @FindBy(xpath = "//span[@class='currency-trigger__text']")
    public WebElement tlButton;

    @FindBy(xpath = "//input[@class='SearchBoxTextEditor SearchBoxTextEditor--autocomplete']")
    public WebElement locationName;

    @FindBy(xpath = "//div[@class='DayPicker-Caption']/div")
    public List<WebElement> dateName;

    @FindBy(xpath = "//span[@aria-label='Next Month']")
    public WebElement nextMonth;

    @FindBy(xpath = "//span[@class='DayPicker-Day__label DayPicker-Day__label--holiday']")
    public List<WebElement> holiday;

    @FindBy(xpath = "*//span[text()='20']")
    public List<WebElement> day;

    @FindBy(xpath = "//div[@data-selenium='occupancyChildren']/span[@data-selenium='plus']")
    public WebElement childPlusButton;

    @FindBy(xpath = "//select[@class='DropdownInput__box']")
    public WebElement childButton;

    @FindBy(xpath = "//option[@class='DropdownInput__option']")
    public List<WebElement> childCount;

    @FindBy(xpath = "//*[@id=\"SearchBoxContainer\"]/div[1]/div/button")
    public WebElement searchButton;

    @FindBy(css = ".ModalMessage.ModalMessage--center")
    public WebElement missingInputContainer;

    @FindBy(css = ".ModalMessage.ModalMessage--center>div")
    public WebElement missingInputContainerInner;
    @FindBy(xpath = "//span[@class='CheckboxContainer-Checkbox Medium']")
    public WebElement forFamilyCheckBox;

    @FindBy(xpath = "//span[@class='Searchbox__searchButton__text']")
    public WebElement searchHotelButton;

    @FindBy(xpath = "//span[@class='header-menu__item--last-row currency__secondary-text'][text()='Avro']")
    public WebElement avroButton;

    @FindBy(xpath = "//*[@data-element-name ='header-flights-links']")
    public WebElement headerFlightsLinks;
}
