package sahibinden.pageobject;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class PersonalInformation
{

    @FindBy(xpath = "//input[@id='fullName']")
    public List<WebElement> fullName;

    @FindBy(xpath = "//button[@type='submit']")
    public WebElement submitButton;

}
