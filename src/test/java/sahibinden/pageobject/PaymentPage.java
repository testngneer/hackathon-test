package sahibinden.pageobject;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class PaymentPage
{
    @FindBy(xpath = "//select[@id='paymentMethod']")
    public WebElement paymentMethod;

    @FindBy(xpath = "//option[text()='Visa / MasterCard / JCB / Amex']")
    public WebElement paymentMethodVisa;

    @FindBy(xpath = "//input[@id='cardNumber']")
    public WebElement cardNumber;

    @FindBy(xpath = "//input[@id='cardHolderName']")
    public WebElement cardHolderName;

    @FindBy(xpath = "//input[@id='expiryDate']")
    public WebElement expiryDate;

    @FindBy(xpath = "//input[@id='cvvCode']")
    public WebElement cvvCode;

    @FindBy(xpath = "//input[@id='isAddCreditCardToFile']")
    public WebElement isAddCreditCardToFile;

    @FindBy(xpath = "//span[text()='Ayırtın & Hemen ödeyin!']")
    public WebElement paymentSubmit;

}
