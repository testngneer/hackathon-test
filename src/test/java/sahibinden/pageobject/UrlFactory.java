package sahibinden.pageobject;

public enum UrlFactory
{

    BASE_URL("https://www.agoda.com/tr-tr/"),
    PARIS_SEARCH_RESULT_PAGE(BASE_URL, "pages/agoda/default/DestinationSearchResult.aspx?city=15470&checkIn=2018-10-18&los=1&rooms=1&adults=2&children=0&cid=-1"),
    SAHIBINDEN_LOGIN_PAGE("https://secure.sahibinden.com/giris"),
    SUPPORT(BASE_URL, "/kurumsal/iletisim/");


    public final String pageUrl;

    UrlFactory(String pageUrl)
    {
        this.pageUrl = pageUrl;
    }

    UrlFactory(UrlFactory baseUrl, String pageUrl)
    {
        this.pageUrl = baseUrl.pageUrl + pageUrl;
    }
}
