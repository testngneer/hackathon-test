package sahibinden.pageobject;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginPage
{

    @FindBy(xpath = "//span[@class='signin-button__text']")
    public WebElement loginButton;

    @FindBy(xpath = "//input[@id='signin-email-input']")
    public WebElement email;

    @FindBy(xpath = "//input[@id='signin-password-input']")
    public WebElement password;

    @FindBy(xpath = "//*[@id=\"recaptcha-anchor\"]/div[5]")
    public WebElement isVisible;

    @FindBy(xpath = "//button[@id='sign-in-submit-button']")
    public WebElement submitButton;

    @FindBy(xpath = "//div[@data-selenium='signin-email-error']")
    public WebElement emailEmptyError;

    @FindBy(xpath = "//div[@data-selenium='signin-password-error']")
    public WebElement passwordEmptyError;
}
