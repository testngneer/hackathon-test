package sahibinden.test;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.commons.util.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import sahibinden.junit.SeleniumExtension;
import sahibinden.junit.SeleniumSession;
import sahibinden.junit.api.*;
import sahibinden.pageobject.AgodaFlightsPage;
import sahibinden.pageobject.AgodaMainPage;
import sahibinden.pageobject.LoginPage;
import sahibinden.pageobject.PersonalInformation;
import sahibinden.pageobject.SearchResultPage;
import sahibinden.pageobject.UrlFactory;

import java.util.ArrayList;
import java.util.logging.Logger;


@SeleniumTestApplication
@ExtendWith(SeleniumExtension.class)
public class AgodaMainTest extends AbstractSeleniumTest
{

    private final static Logger logger = Logger.getLogger(AgodaMainTest.class.getName());
    @PageObject
    private AgodaMainPage agodaMainPage;
    @PageObject
    private SearchResultPage searchResultPage;
    @PageObject
    private LoginPage loginPage;
    @PageObject
    private PersonalInformation personalInformation;

    @PageObject
    private AgodaFlightsPage agodaFlightsPage;

    private static String EMAIL_EMPTY_ERROR_TEXT = "Email gerekiyor.";
    private static String PASSWORD_EMPTY_ERROR_TEXT = "Şifre gereklidir.";

    @BeforeEach
    public void setUp()
    {
        super.setUp();
    }

    private static String MONTH = "Nisan";



    @Test
    @Driver(driver = DriverType.CHROME, fullScreen = true)
    @DisplayName("testSelectMultipleCriteriaForHotelSearchForParis")
    @Description("test covers that if user click search button without necessary form inputs dialog must be shown")
    public void testSelectMultipleCriteriaForHotelSearchForParis(SeleniumSession seleniumSession)
    {
        testSelectMultipleCriteriaForHotelSearch(seleniumSession,"Paris");
    }
    @Test
    @Driver(driver = DriverType.CHROME, fullScreen = true)
    @DisplayName("testSelectMultipleCriteriaForHotelSearchForRoma")
    @Description("test covers that if user click search button without necessary form inputs dialog must be shown")
    public void testSelectMultipleCriteriaForHotelSearchForRoma(SeleniumSession seleniumSession)
    {
        testSelectMultipleCriteriaForHotelSearch(seleniumSession,"Roma");
    }

    @Test
    @Driver(driver = DriverType.CHROME,fullScreen = true)
    @DisplayName("testSearchWithoutFormInputs")
    @Description("test covers that if user click search button without necessary form inputs dialog must be shown")
    public void testSearchWithoutFormInputs()
    {
        browser.get(UrlFactory.BASE_URL.pageUrl);
        browser.click(agodaMainPage.searchButton);

        Assertions.assertTrue(agodaMainPage.missingInputContainer.isDisplayed());
        Assertions.assertTrue(StringUtils.isNotBlank(agodaMainPage.missingInputContainerInner.getText()));
    }

    @Test
    @Driver(driver = DriverType.CHROME, fullScreen = true)
    @DisplayName("testLogin")
    @Description("Login sayfasında must olan input alanların kontrolü")
    public void testLogin(SeleniumSession seleniumSession)
    {
        browser.click(loginPage.loginButton);
        browser.click(loginPage.submitButton);

        Assertions.assertEquals(loginPage.emailEmptyError.getText(), EMAIL_EMPTY_ERROR_TEXT);
        Assertions.assertEquals(loginPage.passwordEmptyError.getText(), PASSWORD_EMPTY_ERROR_TEXT);

    }

    @Description("Search Flights")
    @Test
    @Driver(driver = DriverType.CHROME, fullScreen = true)
    @DisplayName("testSearchFlight")
    public void testSearchFlight()
    {
        browser.click(agodaMainPage.headerFlightsLinks);
        browser.initSeleniumHelper();

        ArrayList<String> tabs2 = new ArrayList<String>(browser.getWindowHandles());
        browser.switchTo().window(tabs2.get(1));

        browser.click(agodaFlightsPage.destination.get(0));
        browser.sendKeys(agodaFlightsPage.destination.get(0), "Paris");
        browser.sendKeys(agodaFlightsPage.destination.get(0), Keys.ENTER);

        browser.sendKeys(agodaFlightsPage.go, "20.04.2019");
        browser.sendKeys(agodaFlightsPage.back,"23.04.2019" );
        browser.click(agodaFlightsPage.searchButton);

        Assertions.assertTrue(searchResultPage.searchResults.size() > 0);
    }


    // ------------------------------------------------------------- >
    private void nextMonth(String month)
    {

        while (!agodaMainPage.dateName.get(0).getText().contains(month))
        {
            browser.click(agodaMainPage.nextMonth);
        }
    }

    private void selectDate()
    {
        nextMonth(MONTH);
        browser.click(agodaMainPage.day.get(0));
        browser.click(agodaMainPage.holiday.get(0));
    }

    private void selectChildCountAndChildAge(int count, String age)
    {
        for (int i = 0; i < count; i++)
        {

            browser.click(agodaMainPage.childPlusButton);
        }
        browser.click(agodaMainPage.childButton);
        agodaMainPage.childButton.findElement(By.cssSelector("option[value='" + age + "']")).click();
    }

    private void changeMoneyCurrency()
    {
        browser.click(agodaMainPage.tlButton);
        browser.click(agodaMainPage.avroButton);

    }

    private void nextMonthForFlight(String mounth)
    {

        while (!agodaFlightsPage.dateName.get(0).getText().contains(mounth))
        {
            browser.click(agodaFlightsPage.nextMonth);
        }
    }

    private void selectLocation(String location)
    {
        browser.sendKeys(agodaMainPage.locationName, location);
        browser.sendKeys(agodaMainPage.locationName, Keys.ENTER);
    }

    private void selectOtherCriteria()
    {
        browser.click(agodaMainPage.forFamilyCheckBox);
        logger.info("aile için kriter seçildi");
        browser.click(agodaMainPage.searchHotelButton);

        searchResultPage.hingPriceInput.clear();
        browser.sendKeys(searchResultPage.hingPriceInput, "500");
        logger.info("en yüksek fiyat eklendi");
        browser.click(searchResultPage.excellentComment);
        logger.info("en iyi yorum kriteri eklendi");
        browser.click(searchResultPage.freeCancellation);
        logger.info("ücretsiz iptal kriteri ekledi");
        //   browser.click(searchResultPage.forFamilyFilter);
        logger.info("aile kriteri eklendi");

        searchResultPage.transportationFilterList.stream().forEach(value -> {
            browser.click(value);
        });
    }


    private void testSelectMultipleCriteriaForHotelSearch(SeleniumSession seleniumSession, String location){
        login(seleniumSession);
        browser.get(UrlFactory.BASE_URL.pageUrl);
        changeMoneyCurrency();
        logger.info("Para Birimi Avro seçildi");

        selectLocation(location);
        selectDate();
        logger.info("istenilen tarihler seçildi");

        selectChildCountAndChildAge(1, "2");
        selectOtherCriteria();

        sleep();
        browser.click(searchResultPage.hotelDetail.get(0));


        ArrayList<String> tabs2 = new ArrayList<String>(browser.getWindowHandles());
        browser.switchTo().window(tabs2.get(1));
        logger.info("Farklı tabe geçildi");

        browser.initSeleniumHelper();
        browser.click(searchResultPage.nowBookButton.get(0));
        logger.info("Hemen ayıt butonuna tıkladı.");

        browser.sendKeys(personalInformation.fullName.get(0), "Test Engineer");
        browser.click(personalInformation.submitButton);

        makePayment(seleniumSession);


    }

}

