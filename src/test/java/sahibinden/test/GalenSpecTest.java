package sahibinden.test;


import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import sahibinden.junit.SeleniumExtension;
import sahibinden.junit.api.Browser;
import sahibinden.junit.api.Driver;
import sahibinden.junit.api.DriverType;
import sahibinden.junit.api.GalenApi;
import sahibinden.junit.api.SeleniumTestApplication;
import sahibinden.junit.core.Agents;
import sahibinden.pageobject.UrlFactory;

import java.io.IOException;
import java.util.Arrays;

@SeleniumTestApplication
@ExtendWith(SeleniumExtension.class)
public class GalenSpecTest {

   @Test
   @Driver(driver = DriverType.CHROME, fullScreen = true)
   @DisplayName("testHomePageLayout")
   public void testHomePageLayout(Browser browser, GalenApi galenApi) throws IOException
   {
      browser.get(UrlFactory.BASE_URL.pageUrl);
      galenApi.checkLayoutDesign("specs/homePage.spec", Arrays.asList("desktop"), this.getClass().getSimpleName());
   }

   @Test
   @Driver(driver = DriverType.CHROME, fullScreen = true)
   @DisplayName("testSearchResultPageLayout")
   public void testSearchResultPageLayout(Browser browser, GalenApi galenApi) throws IOException
   {
      browser.get(UrlFactory.PARIS_SEARCH_RESULT_PAGE.pageUrl);
      galenApi.checkLayoutDesign("specs/searchResultPage.spec", Arrays.asList("desktop"), this.getClass().getSimpleName());
   }

   @Test
   @Driver(driver = DriverType.CHROME, mobileAgent = Agents.Android.ANDROID_4_2_1, width = 320, heigth = 640)
   @DisplayName("testSearchResultPageLayoutForMobileSite")
   public void testSearchResultPageLayoutForMobileSite(Browser browser, GalenApi galenApi) throws IOException
   {
      browser.get(UrlFactory.BASE_URL.pageUrl);
      galenApi.checkLayoutDesign("specs/homePageMobileSite.spec", Arrays.asList("mobile"), this.getClass().getSimpleName());
   }

}
