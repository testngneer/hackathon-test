package sahibinden.test;

import org.junit.jupiter.api.BeforeEach;
import sahibinden.junit.SeleniumSession;
import sahibinden.junit.api.Browser;
import sahibinden.junit.api.Driver;
import sahibinden.junit.api.PageObject;
import sahibinden.pageobject.LoginPage;
import sahibinden.pageobject.PaymentPage;
import sahibinden.pageobject.UrlFactory;

public class AbstractSeleniumTest
{
    @Driver
    public Browser browser;

    @PageObject
    private LoginPage loginPage;

    @PageObject
    private PaymentPage paymentPage;

    @BeforeEach
    public void setUp()
    {
        browser.get(UrlFactory.BASE_URL.pageUrl);
    }

    protected void login(SeleniumSession seleniumSession){
        browser.click(loginPage.loginButton);
        browser.sendKeys(loginPage.email,seleniumSession.getProperty("login.username"));
        browser.sendKeys(loginPage.password,seleniumSession.getProperty("login.password"));
        browser.click(loginPage.submitButton);

        try {
            if(loginPage.isVisible.isDisplayed()){
                browser.click(loginPage.isVisible);
            }
        }catch (Exception e){

        }

    }

    protected void makePayment(SeleniumSession seleniumSession){
        browser.click(paymentPage.paymentMethod);
        browser.click(paymentPage.paymentMethodVisa);
        browser.sendKeys(paymentPage.cardNumber, seleniumSession.getProperty("payment.cardNumber"));
        browser.sendKeys(paymentPage.cardHolderName, seleniumSession.getProperty("payment.cardName"));
        browser.sendKeys(paymentPage.expiryDate, seleniumSession.getProperty("payment.expireDate"));
        browser.sendKeys(paymentPage.cvvCode, seleniumSession.getProperty("payment.cvvCode"));
        browser.click(paymentPage.isAddCreditCardToFile);
        browser.click(paymentPage.paymentSubmit);
    }

    protected void sleep(){
        try
        {
            Thread.sleep(5000);
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }

    }
}
