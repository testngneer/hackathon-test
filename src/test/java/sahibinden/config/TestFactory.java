package sahibinden.config;

import org.openqa.selenium.Platform;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import sahibinden.junit.SeleniumSession;
import sahibinden.junit.api.Driver;
import sahibinden.junit.core.DriverAnnotateWrapper;
import sahibinden.junit.factory.driver.WebDriverFactory;

import java.util.Properties;

public class TestFactory extends WebDriverFactory {

   public TestFactory(SeleniumSession seleniumSession) {
      super(seleniumSession);
   }

   @Override
   public RemoteWebDriver createDriver(Properties properties, DriverAnnotateWrapper driverAnnotateWrapper) throws Exception {
      DesiredCapabilities capabilities = new DesiredCapabilities().chrome();

      capabilities.setCapability("enable-restore-session-state", true);
      capabilities.setJavascriptEnabled(true);
      capabilities.setBrowserName("chrome");
      capabilities.setPlatform(Platform.ANY);

      ChromeOptions chromeOptions = new ChromeOptions();
      chromeOptions.addArguments("--whitelisted-ips");
      chromeOptions.addArguments("--no-sandbox");
      chromeOptions.merge(capabilities);
      chromeOptions.setHeadless(true);

      return new ChromeDriver(chromeOptions);
   }
}
